<?php

namespace Tetrapak07\Multitimezone;

use Carbon\Carbon;

/**
 * Description of DateHelper
 *
 */
class DateHelper
{
   # $addOffset = false -> datetime to default (server) value, so this will use in beforeSave() models method
   # $addOffset = true -> server(default) datetime to user value, so this will use in afterFetch() models method
   /*
    * 
    */
   public static function dateTimeZoneFormat($dateTime = false, $addOffset = false)
   {
       $timezone = new TimeZoneBase();
       $offset = $timezone->userTimeZoneUTC;
       
       if (!$dateTime) {
          $dateTime = Carbon::now(); 
       }
       
       $dt = Carbon::parse($dateTime);
       
       if ($addOffset) {
         $dt->addHours($offset); 
       } else {
         $dt->subHours($offset);     
       }
       
       return $dt;
   }

}
