<?php

namespace Tetrapak07\Multitimezone;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use Carbon\Carbon;

class TimeZoneBase extends Controller
{
    public $defaultTimeZone;
    public $defaultTimeZoneUTC;
    public $userTimeZone;
    public $userTimeZoneUTC;
    
    public function initialize()
    {
       
    }
    
    public function onConstruct()
    {
        $this->dataReturn = true;
        $this->defaultTimeZone = $this->config->modules->multitimezone->defaultTimeZone;
        $this->defaultTimeZoneUTC = $this->config->modules->multitimezone->defaultTimeZoneUTC;
        $this->userTimeZone = $this->defaultTimeZone;
        $this->userTimeZoneUTC = $this->defaultTimeZoneUTC;
       
        if (!SupermoduleBase::checkAndConnectModule('multitimezone')) {
            $this->dataReturn = false;
        } else {
            $this->userTimeZone = Carbon::now()->tzName; 
            $this->userTimeZoneUTC = Carbon::createFromTimestamp(0)->offsetHours;
        }
    }
     
}           
               
